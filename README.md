## Plugins ##
The plugins are basicly implement in the shared library that dyanamically loaded in the main program. If we Want to develop a Dyanamic system the plugins are really
helpful


** Plugin Based system Architecture **

1) Plugin manager

2) The plugins implementation


**Plugin Manager**

It is piece of code which manage the plugins and dynamically load them into the main program and after done the execution it unload the plugins


**Plugin Implementation**

In Plugin implementation the plugin is implemented.


**Above Committs**

1) We created the Data Visualization folder which content the application and all the plugins.

2) We create Project file in the visual studio and named it as the PluginManager.

3) After creating the project the **"framework.h"**,**"pch.h"**,**"dllmain.cpp"** automatically generated in the PluginManager Folder 

4) Then we create the **"PluginManager.h"** header file and then we create the **"PluginManager.c"** in the folder where PluginManger.h Contain Create and destroy value. 

5) After creating the PluginManager we create the new project PluginImplementation.

6) In the PluginImplementation the **point3** files again created automatically in the vstudio 2019 and we Created the **PluginImpl.c"**

7) We gave the path to the **pluginImpl.c** of pluginManager folder header file.


**WE ARE USING THE GCC COMPILER FOR C PROGRAMS AND THE WINDOWS AS OS**

**WE ARE IMPLEMENTING THIS DATA VIZUALIZATION PROJECT IN THE VISUAL STUDIO 2019**